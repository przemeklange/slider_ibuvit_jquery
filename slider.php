<section class="home-slider" id="home-slider">
      <div class="container container-custom">
        <div class="slider">
            <ul class="slide-group">
                <a class="slide-item">
                    <div class="row">
                        <div class="col-sm-12 col-md-5 pt-4">
                            <img src="<?php echo get_template_directory_uri(); ?>/dist/images/group-451-e1611329677718.png" class="mb-3" alt="3W">
                            <h3>Pierwsza i jedyna w Polsce<br><span class="font-weight-normal">innowacyjna technologia</span><br>TRÓJWARSTWOWEJ TABLETKI<br><span class="font-weight-normal">zapewnia optymalne wchłanianie <br>składników</span></h3>
                            <p class="f-12-c-gray">*IBUVIT Witamina C 1000, IBUVIT Multispec zastosowanie innowacyjnej technologii uwalniania może korzystnie wpływać na poprawę wchłaniania składników tabletki. Badanie in vitro - Porównanie uwalniania  i wchłaniania produktu w technologii 3W MULTILAYER vs suplement diety zawierający żelazo w tradycyjnej tabletce w postaci siarczanu żelaza  Badanie in vitro - porównanie uwalniania i wchłaniania IBUVIT Magnez vs suplement diety zawierający magnez w postaci tlenku magnezu (Mag 2 24h)</p>
                            <button onClick="http://ibuvit.pl/produkty/ibuvit-d3-2000-iu/" class="btn btn-third">Sprawdź</button>
                        </div>
                        <div class="col-sm-12 col-md-7">
                            <img src="<?php echo get_template_directory_uri(); ?>/dist/images/1-slider-2-4.png" alt="green" class="position-absolute img09 w-50">
                            <div class="position-absolute left-310">
                                <div class="btn-secound-slide1 f-22 text-center font-weight-bold">1.FAST</div>
                                <div class="text-center f-12 text-white">Warstwa</div>
                                <div class="text-center f-12 text-white font-weight-bold">SZYBKIEGO UWALNIANIA</div>
                            </div>
                            <img src="<?php echo get_template_directory_uri(); ?>/dist/images/1-slider-2-2.png" alt="orange" class="position-absolute img10 w-50">
                            <div class="position-absolute left-310 top-130">
                                <div class="btn-secound-slide2 f-22 text-center font-weight-bold">2.NORMAL</div>
                                <div class="text-center f-12 text-white">Warstwa</div>
                                <div class="text-center f-12 text-white font-weight-bold">NORMALNEGO UWALNIANIA</div>
                            </div>
                            <img src="<?php echo get_template_directory_uri(); ?>/dist/images/1-slider-2-3.png" alt="blue" class="position-absolute img11">
                            <div class="position-absolute left-310 top-260">
                                <div class="btn-secound-slide3 f-22 text-center font-weight-bold">3.RETARD</div>
                                <div class="text-center f-12 text-white">Warstwa</div>
                                <div class="text-center f-12 text-white font-weight-bold">OPÓŹNIONEGO UWALNIANIA</div>
                            </div>
                        </div>
                    </div>
                </a>
                <a class="slide-item">
                    <div class="row">
                        <div class="col-sm-12 col-md-5 pt-4">
                            <img src="<?php echo get_template_directory_uri(); ?>/dist/images/group-451-e1611329677718.png" class="mb-3" alt="3W">
                            <h3><span class="font-weight-normal">Innowacyjne suplementy<br>diety w </span><b>technologii<br>3W MULTILAYER</b><br><span class="font-weight-normal">zapewnia odpowenide <br>wchłanianie składników</span></h3>
                            <p class="f-12-c-gray">*IBUVIT Witamina C 1000, IBUVIT Multispec zastosowanie innowacyjnej technologii uwalniania może korzystnie wpływać na poprawę wchłaniania składników tabletki. Badanie in vitro - Porównanie uwalniania  i wchłaniania produktu w technologii 3W MULTILAYER vs suplement diety zawierający żelazo w tradycyjnej tabletce w postaci siarczanu żelaza  Badanie in vitro - porównanie uwalniania i wchłaniania IBUVIT Magnez vs suplement diety zawierający magnez w postaci tlenku magnezu (Mag 2 24h)</p>
                            <button onClick="http://ibuvit.pl/produkty/ibuvit-d3-2000-iu/" class="btn btn-third">Sprawdź</button>
                        </div>
                        <div class="col-sm-12 col-md-7">
                            <img src="<?php echo get_template_directory_uri(); ?>/dist/images/img05.png" alt="witamina C 1000" class="position-absolute img05">
                            <img src="<?php echo get_template_directory_uri(); ?>/dist/images/ibuvit-MultiSpec.png" alt="multispec" class="w-45 position-absolute img06">
                            <img src="<?php echo get_template_directory_uri(); ?>/dist/images/ibuvit-Magnez.png" alt="magnez" class="position-absolute img07">
                            <img src="<?php echo get_template_directory_uri(); ?>/dist/images/ibuvit-Zelazo+C.png" alt="zelazo + C" class="position-absolute img08">
                            <div class="position-absolute color-black btn-third-slide text-center f-12">Suplement diety</div>
                        </div>
                    </div>
                </a>
                <a href="http://ibuvit.pl/produkty/ibuvit-d3-2000-iu/" class="slide-item">
                    <div class="row">
                        <div class="col-sm-12 col-md-5 pt-5">
                            <img src="<?php echo get_template_directory_uri(); ?>/dist/images/group-451-e1611329677718.png" class="mb-3" alt="3W">
                            <h3><b>Pierwszy i jedyny lek OTC<br>z witaminą D<br></b>
                            <span class="font-weight-normal">w dawce</span> <span class="text-yellow">2000 UI</span> i <span class="has-ibuvit-orange-color">4000 IU</span><br><b>w postaci kapsułek.</b><span class="f-12-c-gray"><sup>1</sup></span></h3>
                            <p class="f-12-c-gray mb-5">Na podstawie ChPL produktów leczniczych ujawnionych<br>w Rejestrze Produktów leczniczych na dzień 8 września 2020r.</p>
                            <button onClick="http://ibuvit.pl/produkty/ibuvit-d3-2000-iu/" class="btn btn-third">Sprawdź</button>
                        </div>
                        <div class="col-sm-12 col-md-7">
                            <img src="<?php echo get_template_directory_uri(); ?>/dist/images/ibuvit-D3-2000-60-thumbnail.png" alt="2000UI" class="w-45 position-absolute img02">
                            <img src="<?php echo get_template_directory_uri(); ?>/dist/images/img-03.png" alt="4000IU" class="w-50 position-absolute img03">
                            <img src="<?php echo get_template_directory_uri(); ?>/dist/images/img-01.png" alt="pastylki" class="position-absolute img01">
                            <img src="<?php echo get_template_directory_uri(); ?>/dist/images/img-04.png" alt="pastylki" class="position-absolute img04">
                            <p class="position-absolute f-18 lotc1">Lek OTC*</p>
                            <p class="position-absolute f-18 lotc2">Lek OTC*</p>
                        </div>
                    </div>
                </a>
            </ul>
        </div>
        <a class="prev mr-5"><img src="<?php echo get_template_directory_uri(); ?>/dist/images/arrow-left.svg"></a>
        <a class="next ml-3"><img src="<?php echo get_template_directory_uri(); ?>/dist/images/arrow-right.svg"></a>
      </div>
    </section>