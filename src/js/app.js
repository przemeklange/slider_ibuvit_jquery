let sliderContainer = $('.slider'),
  slideGroup = $('.slide-group'),
  slideItem = $('.slide-item'),
  prevBtn = $('.prev'),
  nextBtn = $('.next'),
  slideHeight = slideItem.height(),
  slideWidth = slideItem.width(),
  slideCount = slideItem.length,
  totalSlideWidth = slideCount * slideWidth

sliderContainer.css({
  width: slideWidth,
  height: slideHeight
});
slideGroup.css({
  width: totalSlideWidth,
  marginLeft: -slideWidth
});
$('.slide-item:last-child').prependTo(slideGroup);

//var interval;

//interval = setInterval(function(){
// moveLeft();
//}, 5000);



function moveRight() {
  slideGroup.animate({
    left: -slideWidth
  }, 200, function () {
    $('.slide-item:last-child').prependTo(slideGroup);
    slideGroup.css({
      left: ''
    });
  })
}

function moveLeft() {
  slideGroup.animate({
    left: +slideWidth
  }, 200, function () {
    $('.slide-item:first-child').appendTo(slideGroup);
    slideGroup.css({
      left: ''
    });
  })
}

prevBtn.click(function () {
  moveRight();
});

nextBtn.click(function () {
  moveLeft();
});
